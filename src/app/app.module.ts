import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler, Button } from 'ionic-angular';
import { MyApp } from './app.component';
import "reflect-metadata";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BrowserAnimationsModule,NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TerminalService } from 'primeng/components/terminal/terminalservice';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SidebarModule } from 'primeng/sidebar';
import { PanelMenuModule } from 'primeng/panelmenu';
import { LoginPageModule } from '../pages/login/login.module';
import { ElementPageModule } from '../pages/element/element.module';
import { FunctionPageModule } from '../pages/function/function.module';
import { HomePageModule } from '../pages/home/home.module';
import { ItemSetPageModule } from '../pages/itemset/itemset.module';
import { ElementAdjuestPageModule } from '../pages/priceadj/elementadj.module';
import { DefdocRefPageModule } from '../pages/defdocref/defdocref.module';
import {CheckboxModule} from 'primeng/checkbox'
import { PriceAdjuestPageModule } from '../pages/priceadj/priceadj.module';
import { PriceAdjRequestPageModule } from '../pages/priceadj/priceadjrequest.module';
import { FlowgraphPageModule } from '../pages/flowgraph/flowgraph.module';


@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    BrowserAnimationsModule,
    NoopAnimationsModule,
    SidebarModule,
    PanelMenuModule,
    LoginPageModule,
    ElementPageModule,
    ElementAdjuestPageModule,
    PriceAdjuestPageModule,
    FunctionPageModule,
    HomePageModule,
    ItemSetPageModule,
    DefdocRefPageModule,
    CheckboxModule,
    PriceAdjRequestPageModule,
    FlowgraphPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TerminalService,
    InAppBrowser,
  ],
  exports:[
  ]
})
export class AppModule {}
