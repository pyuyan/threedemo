import { Injectable } from "@angular/core";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { MenuController } from "ionic-angular/components/app/menu-controller";

/*
 * 上下文记录存储
*/
@Injectable()
export class ContextData {

    static funcmenu:MenuController;

    public static SetFuncMenuController(menu:MenuController){
        ContextData.funcmenu = menu;
    }

    public static GetFuncMenuController(){
        return ContextData.funcmenu;
    }

    static navctrl:NavController;

    public static SetNavController(nav:NavController){
        ContextData.navctrl = nav;
    }

    public static GetNavController(){
        return ContextData.navctrl;
    }


    static instance: ContextData;
    public static Create() {
        if (!ContextData.instance){
            ContextData.instance = new ContextData();
            ContextData.instance.functree.push(
                {
                    label: '首页',
                    icon: 'fa fa-fw fa-home',
                    funcid: 'HomePage',
                    command:ContextData.instance.NavToFunc
                },
                {
                    label: '价格基础信息',
                    icon: '',
                    items: [
                        {
                            label: '价格因素维护',
                            icon: 'fa fa-fw fa-arrow-circle-right',
                            funcid: 'ElementPage',
                            command:ContextData.instance.NavToFunc
                        },
                    ]
                },
                {
                    label: '价格公式维护',
                    icon: '',
                    items: [
                        {
                            label: '价格公式设计', 
                            icon: 'fa fa-fw fa-arrow-circle-right',
                            funcid: 'FunctionPage',
                            command:ContextData.instance.NavToFunc
                        },
                        {
                            label: '物料-公式配置', 
                            icon: 'fa fa-fw fa-arrow-circle-right',
                            funcid: 'ItemSetPage',
                            command:ContextData.instance.NavToFunc
                        }
                    ]
                },
                {
                    label: '调价管理',
                    icon: '',
                    items: [
                        {
                            label: '价格因素调整单',
                            icon: 'fa fa-fw fa-arrow-circle-right',
                            funcid: 'ElementAdjPage',
                            command:ContextData.instance.NavToFunc
                        },
                        {
                            label: '物料调价申请单', 
                            icon: 'fa fa-fw fa-arrow-circle-right', 
                            funcid: 'PriceAdjRequestPage',
                            command:ContextData.instance.NavToFunc
                        }
                    ]
                },
                {
                    label: '登出',
                    icon: 'fa fa-fw fa-ban',
                    funcid: 'LoginPage',
                    command:ContextData.instance.NavToFunc
                },
            );
        }
        return ContextData.instance;
    }

    functree:Array<any> = new Array<any>();

    NavToFunc(event:any){
        let funcid:any = event.item.funcid;
        ContextData.GetNavController().setRoot(funcid,{});
        ContextData.GetFuncMenuController().close();
    }
}