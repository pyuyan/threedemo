import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ElementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-element',
  templateUrl: 'element.html',
})
export class ElementPage {

  cols:any = [
    {header:'变更值',field:'NewValue'},
    {header:'版本号',field:'Version'},
    {header:'生效时间',field:'EnabledTime'},
    {header:'失效时间',field:'DisabledTime'},
    {header:'创建人',field:'CreatedBy'},
    {header:'创建日期',field:'CreatedOn'},
    {header:'审核人',field:'Approver'},
    {header:'审核日期',field:'ApprovedOn'},
  ];

  tabledata:any = [
    {NewValue:'111',Version:'20180621001',EnabledTime:'2018-06-21 12:12:00',DisabledTime:'2018-11-21 12:12:00',CreatedBy:'张三',CreatedOn:'2018-06-20',Approver:'李四',ApprovedOn:'2018-06-21'},
    {NewValue:'222',Version:'20180621002',EnabledTime:'2018-06-22 12:12:00',DisabledTime:'2018-11-22 12:12:00',CreatedBy:'张三',CreatedOn:'2018-06-20',Approver:'李四',ApprovedOn:'2018-06-21'},
    {NewValue:'333',Version:'20180621003',EnabledTime:'2018-06-23 12:12:00',DisabledTime:'2018-11-23 12:12:00',CreatedBy:'张三',CreatedOn:'2018-06-20',Approver:'李四',ApprovedOn:'2018-06-21'},
    {NewValue:'444',Version:'20180621004',EnabledTime:'2018-06-24 12:12:00',DisabledTime:'2018-11-24 12:12:00',CreatedBy:'张三',CreatedOn:'2018-06-20',Approver:'李四',ApprovedOn:'2018-06-21'},
    {NewValue:'555',Version:'20180621005',EnabledTime:'2018-06-25 12:12:00',DisabledTime:'2018-11-25 12:12:00',CreatedBy:'张三',CreatedOn:'2018-06-20',Approver:'李四',ApprovedOn:'2018-06-21'},
    {NewValue:'666',Version:'20180621006',EnabledTime:'2018-06-26 12:12:00',DisabledTime:'2018-11-26 12:12:00',CreatedBy:'张三',CreatedOn:'2018-06-20',Approver:'李四',ApprovedOn:'2018-06-21'},
    {NewValue:'777',Version:'20180621007',EnabledTime:'2018-06-27 12:12:00',DisabledTime:'2018-11-27 12:12:00',CreatedBy:'张三',CreatedOn:'2018-06-20',Approver:'李四',ApprovedOn:'2018-06-21'},
    {NewValue:'888',Version:'20180621008',EnabledTime:'2018-06-28 12:12:00',DisabledTime:'2018-11-28 12:12:00',CreatedBy:'张三',CreatedOn:'2018-06-20',Approver:'李四',ApprovedOn:'2018-06-21'},
  ];

  putypes:any = [
    {name: '压铸件', code: '压铸件'},
    {name: '涤纶布', code: '涤纶布'},
  ];

  elementtypes:any = [
    {name: '原料铝', code: '原料铝'},
    {name: '原料铜', code: '原料铜'},
    {name: '涤纶布', code: '涤纶布'},
    {name: '包装材料', code: '包装材料'},
  ];

  areatypes:any = [
    {name: '公共', code: '公共'},
    {name: '物料', code: '物料'},
    {name: '供应商', code: '供应商'},
    {name: '物料+供应商', code: '物料+供应商'},
  ];

  valuetypes:any = [
    {name: '文本', code: '文本'},
    {name: '数值', code: '数值'},
    {name: '布尔', code: '布尔'},
    {name: '实体', code: '实体'},
    {name: '集合', code: '集合'},
    {name: '公式', code: '公式'},
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ElementPage');
  }

}
