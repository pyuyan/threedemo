import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElementPage } from './element';
import { ButtonModule } from 'primeng/primeng';
import {InputTextModule} from 'primeng/inputtext';
import {CardModule} from 'primeng/card';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {EditorModule} from 'primeng/editor';
import {DataViewModule} from 'primeng/dataview';
import { TableModule } from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';

@NgModule({
  declarations: [
    ElementPage,
  ],
  imports: [
    IonicPageModule.forChild(ElementPage),
    TableModule,
    ButtonModule,
    InputTextModule,
    CardModule,
    InputTextareaModule,
    EditorModule,
    DataViewModule,
    DropdownModule
  ],
  entryComponents: [
    ElementPage
  ],
  exports:[
    ElementPage
  ]
})
export class ElementPageModule {}
