import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FunctionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-function',
  templateUrl: 'function.html',
})
export class FunctionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FunctionPage');
  }

  putypes:any = [
    {name: '压铸件', code: '压铸件'},
    {name: '涤纶布', code: '涤纶布'},
  ];

  elements:any = [
    {
      label:'公共',
      data:'Public',
      expandedIcon:'fa fa-bars',
      collapsedIcon: "fa fa-arrow-circle-down",
      expanded:true,
      nodetype:'dir',
      children:[
        {
          label:'铜价',
          data:'BracePrice',
          icon:'fa fa-star-o',
          nodetype:'obj',
        },
        {
          label:'铝价',
          data:'BracePrice',
          icon:'fa fa-star-o',
          nodetype:'obj',
        }
      ]
    },
    {
      label:'所属分类',
      data:'Kind',
      expandedIcon:'fa fa-bars',
      collapsedIcon: "fa fa-arrow-circle-down",
      expanded:true,
      nodetype:'dir',
      children:[
        {
          label:'单重',
          data:'Weight',
          icon:'fa fa-star-o',
          nodetype:'obj',
        },
        {
          label:'工序',
          data:'Process',
          icon:'fa fa-star-o',
          nodetype:'obj',
        },
        {
          label:'表面处理',
          data:'Surface',
          icon:'fa fa-star-o',
          nodetype:'obj',
        }
      ]
    },
    {
      label:'上下文',
      data:'Context',
      expandedIcon:'fa fa-bars',
      collapsedIcon: "fa fa-arrow-circle-down",
      expanded:true,
      nodetype:'dir',
      children:[
        {
          label:'当前年度',
          data:'CurrYear',
          icon:'fa fa-star-o',
          nodetype:'obj',
        },
        {
          label:'当前月度',
          data:'CurrMonth',
          icon:'fa fa-star-o',
          nodetype:'obj',
        },
        {
          label:'当前日',
          data:'CurrDay',
          icon:'fa fa-star-o',
          nodetype:'obj',
        },
        {
          label:'当前时间',
          data:'NowTime',
          icon:'fa fa-star-o',
          nodetype:'obj',
        },
        {
          label:'登录组织',
          data:'CurrOrg',
          icon:'fa fa-star-o',
          nodetype:'obj',
        },
        {
          label:'登录人员',
          data:'CurrUser',
          icon:'fa fa-star-o',
          nodetype:'obj',
        }
      ]
    }
  ]

  functions:any = [
    {
      label:'通用',
      data:'Common',
      expandedIcon:'fa fa-bars',
      collapsedIcon: "fa fa-arrow-circle-down",
      expanded:true,
      nodetype:'dir',
      children:[
        {
          label:'IIF',
          data:'IIF(condition,truevalue,falsevalue)',
          icon:'fa fa-star-o',
          nodetype:'obj',
		      description:'IIF三元逻辑判断\nIIF(条件表达式,真执行,假执行)\n\nEXP:IIF(A==0,"TRUE","FALSE")'
        },
        {
          label:'IsNull',
          data:'IsNull(value)',
          icon:'fa fa-star-o',
          nodetype:'obj',
		      description:'空值判断'
        },
        {
          label:'Each',
          data:'Each(arraytype,filter)',
          icon:'fa fa-star-o',
          nodetype:'obj',
		      description:'数组对象遍历取值'
        }
      ]
    },
    {
      label:'文本函数',
      data:'Text',
      expandedIcon:'fa fa-bars',
      collapsedIcon: "fa fa-arrow-circle-down",
      expanded:false,
      nodetype:'dir',
      children:[
        {
          label:'SUBSTR',
          data:'SUBSTR(value,startIndex,endIndex)',
          icon:'fa fa-star-o',
          nodetype:'obj',
		      description:'字符串截取'
        },
        {
          label:'REPLACE',
          data:'REPLACE(value,oldchar,newchar)',
          icon:'fa fa-star-o',
          nodetype:'obj',
		      description:'字符串元素替换'
        },
        {
          label:'SPLIT',
          data:'SPLIT(value,char)',
          icon:'fa fa-star-o',
          nodetype:'obj',
		      description:'字符串分割'
        },
        {
          label:'INDEXOF',
          data:'INDEXOF(value,char)',
          icon:'fa fa-star-o',
          nodetype:'obj',
		      description:'字符串查询，返回位置int值'
        }
      ]
    },
    {
      label:'数值函数',
      data:'Number',
      expandedIcon:'fa fa-bars',
      collapsedIcon: "fa fa-arrow-circle-down",
      expanded:false,
      nodetype:'dir',
      children:[
        {
          label:'ROUND',
          data:'ROUND(value,pre)',
          icon:'fa fa-star-o',
          nodetype:'obj',
		      description:'四舍五入'
        },
        {
          label:'MOD',
          data:'MOD(value)',
          icon:'fa fa-star-o',
          nodetype:'obj',
		      description:'数值取模'
        },
        {
          label:'ABS',
          data:'ABS(value)',
          icon:'fa fa-star-o',
          nodetype:'obj',
		      description:'取绝对值'
        },
        {
          label:'Ceiling',
          data:'Ceiling(value)',
          icon:'fa fa-star-o',
          nodetype:'obj',
		      description:'向下取整'
        },
        {
          label:'Floor',
          data:'Floor(value)',
          icon:'fa fa-star-o',
          nodetype:'obj',
		      description:'向上取整'
        }
      ]
    }
  ]



  onElementSelected(event:any){
    if(event&&event.node.nodetype==='obj'){
      this.formula = this.formula + '#' + event.node.label + '#';
    }
  }

  onFunctionSelected(event:any){
    if(event&&event.node.nodetype==='obj'){
      this.formula = this.formula + event.node.data;
    }
  }

  addOperator(oper:string){
    this.formula = this.formula + oper;
  }

  functionobj:any = {
    description:''
  };

  elementobj:any = {

  };

  formula:any = '';

}
