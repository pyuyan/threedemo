import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FunctionPage } from './function';
import { ButtonModule } from 'primeng/primeng';
import {InputTextModule} from 'primeng/inputtext';
import {CardModule} from 'primeng/card';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {EditorModule} from 'primeng/editor';
import {DataViewModule} from 'primeng/dataview';
import { TableModule } from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import {TreeModule} from 'primeng/tree';
import {CheckboxModule} from 'primeng/checkbox'


@NgModule({
  declarations: [
    FunctionPage,
  ],
  imports: [
    IonicPageModule.forChild(FunctionPage),
    TableModule,
    ButtonModule,
    InputTextModule,
    CardModule,
    InputTextareaModule,
    EditorModule,
    DataViewModule,
    DropdownModule,
    TreeModule,
    CheckboxModule
  ],
  entryComponents: [
    FunctionPage
  ],
  exports:[
    FunctionPage
  ]
})
export class FunctionPageModule {}
