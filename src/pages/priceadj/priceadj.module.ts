import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ButtonModule } from 'primeng/primeng';
import {InputTextModule} from 'primeng/inputtext';
import {CardModule} from 'primeng/card';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {EditorModule} from 'primeng/editor';
import {DataViewModule} from 'primeng/dataview';
import { TableModule } from 'primeng/table';
import { PriceAdjPage } from './priceadj';
import {DropdownModule} from 'primeng/dropdown';

@NgModule({
  declarations: [
    PriceAdjPage,
  ],
  imports: [
    IonicPageModule.forChild(PriceAdjPage),
    TableModule,
    ButtonModule,
    InputTextModule,
    CardModule,
    InputTextareaModule,
    EditorModule,
    DataViewModule,
    DropdownModule
  ],
  entryComponents: [
    PriceAdjPage,
  ],
  exports: [
    PriceAdjPage,
  ]
})
export class PriceAdjuestPageModule {}