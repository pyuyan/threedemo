import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ButtonModule } from 'primeng/primeng';
import {InputTextModule} from 'primeng/inputtext';
import {CardModule} from 'primeng/card';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {EditorModule} from 'primeng/editor';
import {DataViewModule} from 'primeng/dataview';
import { TableModule } from 'primeng/table';
import { ElementAdjPage } from './elementadj';
import {DropdownModule} from 'primeng/dropdown';
import {DialogModule} from 'primeng/dialog';

@NgModule({
  declarations: [
    ElementAdjPage
  ],
  imports: [
    IonicPageModule.forChild(ElementAdjPage),
    TableModule,
    ButtonModule,
    InputTextModule,
    CardModule,
    InputTextareaModule,
    EditorModule,
    DataViewModule,
    DropdownModule,
    DialogModule
  ],
  entryComponents: [
    ElementAdjPage
  ],
  exports: [
    ElementAdjPage
  ]
})
export class ElementAdjuestPageModule {}