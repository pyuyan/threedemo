import { Component, ViewChild, ElementRef } from '@angular/core';
import { ContextData } from '../../app/ContextData';
import { IonicPage } from 'ionic-angular/navigation/ionic-page';
import { ModalController } from 'ionic-angular';
import { FlowgraphPage } from '../flowgraph/flowgraph';

@IonicPage()
@Component({
  selector: 'page-elementadj',
  templateUrl: 'elementadj.html',
})
export class ElementAdjPage {

  @ViewChild('formuladiv') formuladiv:ElementRef;

    formula:any = 'IIF(#手输单价#==0,(#铜价#*#单重#+(#费用A#)+IFF(#表面处理# like "%防锈%",30,0)),#手输单价#)+Each(#工序#,"1=1")';
  
    elementdatas:any = [
      {
        ElementType:'材料',
        Code:'D001',
        Name:'铜价',
        Value:10,
        DataType:'Number',
        Version:'2018062111001'
      },
      {
        ElementType:'费用',
        Code:'D002',
        Name:'公共费用A',
        Value:12,
        DataType:'Number',
        Version:'2018062111001'
      },
      {
        ElementType:'费用',
        Code:'D003',
        Name:'工序',
        Value:82,
        DataType:'Array',
        Version:'2018062111001'
      },
    ];

    effectives:any = [
      {name: '立即生效(全局)', code: '立即生效(全局)'},
      {name: '立即生效(所选范围)', code: '立即生效(所选范围)'},
      {name: '不计算', code: '不计算'},
    ];

    formulaMetadatas:Array<any> = [
      {
        FieldCode:'TEST1',
        FieldName:'手输单价',
        DataType:'Number',
        Value:'11',
        Disabled:true
      },
      {
        FieldCode:'TEST2',
        FieldName:'铜价',
        DataType:'Number',
        Value:'22',
        Disabled:false
      },
      {
        FieldCode:'TEST3',
        FieldName:'单重',
        DataType:'Number',
        Value:'33',
        Disabled:false
      },
      {
        FieldCode:'TEST4',
        FieldName:'费用A',
        DataType:'Ref',
        Value:'',
        Disabled:false
      },
      {
        FieldCode:'TEST5',
        FieldName:'表面处理',
        DataType:'Text',
        Value:'',
        Disabled:false
      },
      {
        FieldCode:'TEST6',
        FieldName:'手输单价',
        DataType:'Number',
        Value:'',
        Disabled:false
      },
      {
        FieldCode:'TEST7',
        FieldName:'工序',
        DataType:'Array',
        Headers:[
          {
            FieldCode:'Code',
            FieldName:'编码',
            DataType:'Text',
            Disabled:true
          },
          {
            FieldCode:'Name',
            FieldName:'名称',
            DataType:'Ref',
            Disabled:true
          },
          {
            FieldCode:'Value',
            FieldName:'数值',
            DataType:'Number',
            Disabled:false
          }
        ],
        Values:[
          {Code:'P01',Name:'工序1',Value:10},
          {Code:'P02',Name:'工序2',Value:20},
          {Code:'P03',Name:'工序3',Value:12}
        ],
        Value:'42',
        Disabled:false
      },
    ];
  
    contextdata:ContextData;

    constructor(public modalctrl:ModalController) {
      this.contextdata = ContextData.Create();
    }

    
  cols:any = [
    { field: '物料编码', header: '物料编码' },
    { field: '物料名称', header: '物料名称' },
    { field: '单价', header: '单价' },
    { field: '计算版本', header: '计算版本' }
  ];

  suppliercols:any = [
    { field: '供应商编码', header: '供应商编码' },
    { field: '供应商名称', header: '供应商名称' },
  ];

  /**
   * 显示流程状态
   */
  showFlowStatus(){
    const modal = this.modalctrl.create(FlowgraphPage);
    modal.present();
  }


  selectedSuppliers:Array<any> = [];

  selectedDatas:Array<any> = [];

  supplierDatas:Array<any> = [
    {"供应商编码":"0001","供应商名称":"XXX公司"},
    {"供应商编码":"0002","供应商名称":"XXX公司"},
    {"供应商编码":"0003","供应商名称":"XXX公司"},
    {"供应商编码":"0004","供应商名称":"XXX公司"},
    {"供应商编码":"0005","供应商名称":"XXX公司"},
  ];

  tabledata:any = [
    {"物料编码":"a1653d4d","物料名称":"VW1998","规格":"White","单价":30.10,"计算版本":"20180503121001898001"},
    {"物料编码":"ddeb9b10","物料名称":"Mercedes1985","规格":"Green","单价":30.25,"计算版本":"20180503121001898001"},
    {"物料编码":"d8ebe413","物料名称":"Jaguar1979","规格":"Silver","单价":30.30,"计算版本":"20180503121001898001"},
    {"物料编码":"aab227b7","物料名称":"Audi1970","规格":"Black","单价":30.12,"计算版本":"20180503121001898001"},
    {"物料编码":"631f7412","物料名称":"Volvo1992","规格":"Red","单价":30.15500,"计算版本":"20180503121001898001"},
    {"物料编码":"7d2d22b0","物料名称":"VW1993","规格":"Maroon","单价":30.40,"计算版本":"20180503121001898001"},
    {"物料编码":"50e900ca","物料名称":"Fiat1964","规格":"Blue","单价":30.25,"计算版本":"20180503121001898001"},
    {"物料编码":"4bbcd603","物料名称":"Renault1983","规格":"Maroon","单价":30.22,"计算版本":"20180503121001898001"},
    {"物料编码":"70214c7e","物料名称":"Renault1961","规格":"Black","单价":30.19,"计算版本":"20180503121001898001"},
    {"物料编码":"ec229a92","物料名称":"Audi1984","规格":"Brown","单价":30.36,"计算版本":"20180503121001898001"},
    {"物料编码":"1083ee40","物料名称":"VW1984","规格":"Silver","单价":30.215,"计算版本":"20180503121001898001"},
    {"物料编码":"6e0da3ab","物料名称":"Volvo1987","规格":"Silver","单价":30.32,"计算版本":"20180503121001898001"},
    {"物料编码":"5aee636b","物料名称":"Jaguar1995","规格":"Maroon","单价":30.20,"计算版本":"20180503121001898001"},
    {"物料编码":"7cc43997","物料名称":"Jaguar1984","规格":"Orange","单价":30.14,"计算版本":"20180503121001898001"},
    {"物料编码":"88ec9f66","物料名称":"Honda1989","规格":"Maroon","单价":30.36,"计算版本":"20180503121001898001"},
    {"物料编码":"f5a4a5f5","物料名称":"BMW1986","规格":"Blue","单价":30.28,"计算版本":"20180503121001898001"},
    {"物料编码":"15b9a5c9","物料名称":"Mercedes1986","规格":"Orange","单价":30.14,"计算版本":"20180503121001898001"},
    {"物料编码":"f7e18d01","物料名称":"Mercedes1991","规格":"White","单价":30.25,"计算版本":"20180503121001898001"},
    {"物料编码":"cec593d7","物料名称":"VW1992","规格":"Blue","单价":30.36,"计算版本":"20180503121001898001"},
    {"物料编码":"d5bac4f0","物料名称":"Renault2001","规格":"Blue","单价":30.25,"计算版本":"20180503121001898001"},
    {"物料编码":"56b527c8","物料名称":"Jaguar1990","规格":"Yellow","单价":30.52,"计算版本":"20180503121001898001"},
    {"物料编码":"1ac011ff","物料名称":"Audi1966","规格":"Maroon","单价":30.45,"计算版本":"20180503121001898001"},
    {"物料编码":"fc074185","物料名称":"BMW1962","规格":"Blue","单价":30.54,"计算版本":"20180503121001898001"},
    {"物料编码":"606ba663","物料名称":"Honda1982","规格":"Blue","单价":30.22,"计算版本":"20180503121001898001"},
    {"物料编码":"d05060b8","物料名称":"Mercedes2003","规格":"Silver","单价":30.15,"计算版本":"20180503121001898001"},
    {"物料编码":"46e4bbe8","物料名称":"Mercedes1986","规格":"White","单价":30.18,"计算版本":"20180503121001898001"},
    {"物料编码":"c29da0d7","物料名称":"BMW1983","规格":"Brown","单价":30.32,"计算版本":"20180503121001898001"},
    {"物料编码":"24622f70","物料名称":"VW1973","规格":"Maroon","单价":30.36,"计算版本":"20180503121001898001"},
    {"物料编码":"7f573d2c","物料名称":"Mercedes1991","规格":"Red","单价":30.21,"计算版本":"20180503121001898001"},
    {"物料编码":"b69e6f5c","物料名称":"Jaguar1993","规格":"Yellow","单价":30.16,"计算版本":"20180503121001898001"},
    {"物料编码":"ead9bf1d","物料名称":"Fiat1968","规格":"Maroon","单价":30.43,"计算版本":"20180503121001898001"},
    {"物料编码":"bc58113e","物料名称":"Renault1981","规格":"Silver","单价":30.36,"计算版本":"20180503121001898001"},
    {"物料编码":"2989d5b1","物料名称":"Honda2006","规格":"Blue","单价":30.240,"计算版本":"20180503121001898001"},
    {"物料编码":"c243e3a0","物料名称":"Fiat1990","规格":"Maroon","单价":30.15,"计算版本":"20180503121001898001"},
    {"物料编码":"e3d3ebf3","物料名称":"Audi1996","规格":"White","单价":30.28,"计算版本":"20180503121001898001"},
    {"物料编码":"45337e7a","物料名称":"Mercedes1982","规格":"Blue","单价":30.14,"计算版本":"20180503121001898001"},
    {"物料编码":"36e9cf7e","物料名称":"Fiat2","规格":"Orange","单价":30.26,"计算版本":"20180503121001898001"},
    {"物料编码":"036bf135","物料名称":"Mercedes1973","规格":"Black","单价":30.22,"计算版本":"20180503121001898001"},
    {"物料编码":"ad612e9f","物料名称":"Mercedes1975","规格":"Red","单价":30.45,"计算版本":"20180503121001898001"},
    {"物料编码":"97c6e1e9","物料名称":"Volvo1967","规格":"Green","单价":30.42,"计算版本":"20180503121001898001"},
    {"物料编码":"ae962274","物料名称":"Volvo1982","规格":"Red","单价":30.36,"计算版本":"20180503121001898001"},
    {"物料编码":"81f8972a","物料名称":"BMW2007","规格":"Black","单价":30.56,"计算版本":"20180503121001898001"},
    {"物料编码":"f8506743","物料名称":"Audi1975","规格":"Blue","单价":30.42,"计算版本":"20180503121001898001"},
    {"物料编码":"596859d1","物料名称":"Fiat2002","规格":"Green","单价":30.48,"计算版本":"20180503121001898001"},
    {"物料编码":"d83c1d9a","物料名称":"Volvo1972","规格":"Black","单价":30.29,"计算版本":"20180503121001898001"},
    {"物料编码":"32f41550","物料名称":"Mercedes1978","规格":"Brown","单价":30.17,"计算版本":"20180503121001898001"},
    {"物料编码":"c28cd2e4","物料名称":"Volvo1982","规格":"Silver","单价":30.24,"计算版本":"20180503121001898001"},
    {"物料编码":"80890dcc","物料名称":"Audi1962","规格":"White","单价":30.36,"计算版本":"20180503121001898001"},
    {"物料编码":"4bf1aeb5","物料名称":"VW2","规格":"Silver","单价":30.24,"计算版本":"20180503121001898001"},
    {"物料编码":"45ca4786","物料名称":"BMW1995","规格":"Maroon","单价":30.50,"计算版本":"20180503121001898001"}
];  
  
    /**
     * 选择公式
     */
    changeFormula(){
      if(event){

      }
    }
  
    /**
     * 内容初始化
     */
    ionViewDidEnter(){
  
    }
  
    ionViewDidLeave() {
  
    }
  
    areatypes:any = [
      {name: '公共', code: '公共'},
      {name: '物料', code: '物料'},
      {name: '供应商', code: '供应商'},
      {name: '物料+供应商', code: '物料+供应商'},
    ];
}
