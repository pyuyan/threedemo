import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DefdocRefPage } from './defdocref';

@NgModule({
  declarations: [
    DefdocRefPage,
  ],
  imports: [
    IonicPageModule.forChild(DefdocRefPage),
  ],
})
export class DefdocRefPageModule {}
