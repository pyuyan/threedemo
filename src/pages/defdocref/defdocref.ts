import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DefdocRefPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-defdocref',
  templateUrl: 'defdocref.html',
})
export class DefdocRefPage {

  refTableHeaders:any = [
    {display:'编码',field:'Code'},
    {display:'名称',field:'Name'},
    {display:'数值',field:'Value'},
    {display:'备注',field:'Desc'},
  ];

  defdocdatas:Array<any> = [];

  constructor(public navCtrl: NavController, 
    public navParams: NavParams) {
      

  }

  ionViewDidLoad() {

  }

}
