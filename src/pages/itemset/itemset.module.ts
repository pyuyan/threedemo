import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ButtonModule } from 'primeng/primeng';
import {InputTextModule} from 'primeng/inputtext';
import {CardModule} from 'primeng/card';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {EditorModule} from 'primeng/editor';
import {DataViewModule} from 'primeng/dataview';
import { TableModule } from 'primeng/table';
import { ItemSetPage } from './itemset';
import {CheckboxModule} from 'primeng/checkbox'

@NgModule({
  declarations: [
    ItemSetPage
  ],
  imports: [
    IonicPageModule.forChild(ItemSetPage),
    TableModule,
    ButtonModule,
    InputTextModule,
    CardModule,
    InputTextareaModule,
    EditorModule,
    DataViewModule,
    CheckboxModule
  ],
  entryComponents: [
    ItemSetPage
  ],
  exports: [
    ItemSetPage
  ]
})
export class ItemSetPageModule {}