import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ContextData } from '../../app/ContextData';
import { IonicPage } from 'ionic-angular/navigation/ionic-page';

@IonicPage()
@Component({
  selector: 'page-itemset',
  templateUrl: 'itemset.html',
})
export class ItemSetPage {

  @ViewChild('formuladiv') formuladiv:ElementRef;

  formula:any = 'IIF(#手输单价#==0,(#铜价#*#单重#+(#费用A#)+IFF(#表面处理# like "%防锈%",30,0)),#手输单价#)+Each(#工序#,"1=1")';

  formulaMetadatas:Array<any> = [];

  contextdata:ContextData;

  constructor(public navCtrl: NavController) {
    this.contextdata = ContextData.Create();
  }

  supplierpricedata:any = {
    headers:[
      {header:'供应商',field:'Supplier'},
      {header:'指导价格',field:'DirectPrice'},
      {header:'价格版本',field:'PriceVersion'}
    ],
    datas:[
      {Supplier:'供应商A',DirectPrice:100,PriceVersion:'201806220001'},
      {Supplier:'供应商B',DirectPrice:100,PriceVersion:'201806220001'},
      {Supplier:'供应商C',DirectPrice:100,PriceVersion:'201806220001'},
      {Supplier:'供应商D',DirectPrice:100,PriceVersion:'201806220001'},
    ]
  };

  /**
   * 选择公式
   */
  changeFormula(){
    if(event){
      document.getElementById
      this.formulaMetadatas = [
        {
          FieldCode:'TEST1',
          FieldName:'手输单价',
          DataType:'Number',
          Value:'11',
          Disabled:true
        },
        {
          FieldCode:'TEST2',
          FieldName:'铜价',
          DataType:'Number',
          Value:'22',
          Disabled:false
        },
        {
          FieldCode:'TEST3',
          FieldName:'单重',
          DataType:'Number',
          Value:'33',
          Disabled:false
        },
        {
          FieldCode:'TEST4',
          FieldName:'费用A',
          DataType:'Ref',
          Value:'',
          Disabled:false
        },
        {
          FieldCode:'TEST5',
          FieldName:'表面处理',
          DataType:'Text',
          Value:'',
          Disabled:false
        },
        {
          FieldCode:'TEST6',
          FieldName:'手输单价',
          DataType:'Number',
          Value:'',
          Disabled:false
        },
        {
          FieldCode:'TEST7',
          FieldName:'工序',
          DataType:'Array',
          Headers:[
            {
              FieldCode:'Code',
              FieldName:'编码',
              DataType:'Text',
              Disabled:true
            },
            {
              FieldCode:'Name',
              FieldName:'名称',
              DataType:'Ref',
              Disabled:true
            },
            {
              FieldCode:'Value',
              FieldName:'数值',
              DataType:'Number',
              Disabled:false
            }
          ],
          Values:[
            {Code:'P01',Name:'工序1',Value:10},
            {Code:'P02',Name:'工序2',Value:20},
            {Code:'P03',Name:'工序3',Value:12}
          ],
          Value:'42',
          Disabled:false
        },
      ];
    }
  }

  busyflag:boolean = false;

  /**
   * 内容初始化
   */
  ionViewDidEnter(){

  }

  ionViewDidLeave() {

  }
}
