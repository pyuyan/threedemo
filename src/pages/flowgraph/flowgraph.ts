import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';



@IonicPage()
@Component({
  selector: 'page-flowgraph',
  templateUrl: 'flowgraph.html',
})
export class FlowgraphPage {

  @ViewChild('flowdiv') flowdiv:ElementRef;

  flowdata:any = {
    title: {
        text: '流程示例'
    },
    tooltip: {
        
    },
    animationDurationUpdate: 1500,
    animationEasingUpdate: 'quinticInOut',
    series : [
        {
            type: 'graph',
            layout: 'none',
            symbolSize: 60,
            roam: true,
            label: {
                normal: {
                    show: true
                }
            },
            edgeSymbol: ['circle', 'arrow'],
            edgeSymbolSize: [4, 10],
            edgeLabel: {
                normal: {
                    textStyle: {
                        fontSize: 12
                    }
                }
            },
            data: [{
                name: '提交',
                x: 300,
                y: 300,
                itemStyle: {
                    normal: {
                        color: '#19c719'
                    }
                },
                flowdetail:[
                  {
                    Sender:'张三',
                    SendTime:'2018-06-23 12:22:10',
                    Operator:'张三',
                    Status:'通过',
                    NodeType:'提交',
                    FlowNote:'',
                    Weight:1
                  }
                ]
            }, {
                name: '主管审核',
                x: 450,
                y: 300,
                itemStyle: {
                    normal: {
                        color: '#19c719'
                    }
                },
                flowdetail:[
                  {
                    Sender:'张三',
                    SendTime:'2018-06-23 12:22:10',
                    Operator:'李四',
                    Status:'通过',
                    NodeType:'审核',
                    FlowNote:'审核通过',
                    Weight:1
                  }
                ] 
            }, {
                name: '部门经理审核',
                x: 600,
                y: 300,
                itemStyle: {
                    normal: {
                        color: '#FF7733'
                    }
                },
                flowdetail:[
                  {
                    Sender:'张三',
                    SendTime:'2018-06-23 12:22:10',
                    Operator:'李四',
                    Status:'通过',
                    NodeType:'审批',
                    FlowNote:'审批通过',
                    Weight:0.5
                  },
                  {
                    Sender:'张三',
                    SendTime:'2018-06-23 12:22:10',
                    Operator:'王五',
                    Status:'审批中',
                    NodeType:'审批',
                    FlowNote:'',
                    Weight:0.5
                  }
                ] 
            },{
                name: '结束',
                x: 750,
                y: 300,
                flowdetail:[

                ]
            }],

            links: [{
                source: '提交',
                target: '主管审核',
                symbolSize: [5, 20],
                label: {
                    normal: {
                        show: true
                    }
                }
            }, {
                source: '主管审核',
                target: '部门经理审核',
                label: {
                    normal: {
                        show: true
                    }
                },
            }, {
                source: '部门经理审核',
                target: '结束',
                label: {
                    normal: {
                        show: true
                    }
                },
            }],
            lineStyle: {
                normal: {
                    opacity: 0.9,
                    width: 2,
                    curveness: 0
                }
            }
        }
    ]
  };

  selectedNode:any = {
    headers:[
      {field:'Sender',fieldname:'发送人'},
      {field:'SendTime',fieldname:'发送时间'},
      {field:'Operator',fieldname:'操作人'},
      {field:'NodeType',fieldname:'节点类型'},
      {field:'Status',fieldname:'执行状态'},
      {field:'FlowNote',fieldname:'流程批语'},
      {field:'Weight',fieldname:'权重'},
    ],
    values:[

    ] 
  };


  constructor(public navCtrl: NavController, public navParams: NavParams,public viewctrl:ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FlowgraphPage');
  }


  onNodeClick(ev:any){
    console.log(ev);

    this.selectedNode.values = ev.data.flowdetail;
  }

  closeDialog(){
    this.viewctrl.dismiss();
  }
}
