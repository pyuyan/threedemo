import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FlowgraphPage } from './flowgraph';
import { NgxEchartsModule } from 'ngx-echarts';
import {DialogModule} from 'primeng/dialog';
import { TableModule } from 'primeng/table';

@NgModule({
  declarations: [
    FlowgraphPage,
  ],
  imports: [
    IonicPageModule.forChild(FlowgraphPage),
    NgxEchartsModule,
    DialogModule,
    TableModule
  ],
})
export class FlowgraphPageModule {}
