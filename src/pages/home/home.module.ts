import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { ButtonModule } from 'primeng/primeng';
import {InputTextModule} from 'primeng/inputtext';
import {CardModule} from 'primeng/card';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {EditorModule} from 'primeng/editor';
import {DataViewModule} from 'primeng/dataview';
import { TableModule } from 'primeng/table';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    TableModule,
    ButtonModule,
    InputTextModule,
    CardModule,
    InputTextareaModule,
    EditorModule,
    DataViewModule,
  ],
})
export class HomePageModule {}
