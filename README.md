# threedemo

#### 项目介绍
采购价格管理系统前端UI演示

#### 软件架构
ionic 3.19
angular 5.10
primeng 6.0
echarts
ngx-echarts

#### 安装教程


```
git clone https://gitee.com/pyuyan/threedemo.git
cd threedemo
npm install
```


#### 使用说明
IONIC引入PRIMENG UI库方法:

```
	npm install primeng@6.0.0 --save
	npm install @angular/animations@5.2.10 --save
	npm install @angular/router@5.2.11 --save
	npm install font-awesome@4.7.0 --save
	npm install primeicons --save
```


	新建文件夹assets\primecss
	复制node_modules\primeicons文件夹到premecss下
	复制node_modules\primeng\resources\themes\omega\文件夹下所有内容到premecss下

index.html内添加

```
  <link rel="stylesheet" type="text/css" href="assets/primecss/primeicons/primeicons.css" />
  <link rel="stylesheet" type="text/css" href="assets/primecss/theme.css" />
  <link rel="stylesheet" type="text/css" href="assets/primecss/primeng.min.css" />
  <link rel="stylesheet" type="text/css" href="assets/primecss/font-awesome.min.css" />
```


app.module.ts添加引用

```
import { BrowserAnimationsModule,NoopAnimationsModule } from '@angular/platform-browser/animations';  
```

然后根据需要引用primeui相关module，参考https://www.primefaces.org/primeng/



